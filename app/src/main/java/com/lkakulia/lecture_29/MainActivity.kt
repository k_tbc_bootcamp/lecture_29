package com.lkakulia.lecture_29

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private val bands = mutableListOf<BandModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        HttpRequest.getRequest(HttpRequest.MAIN_INFO, object: CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@MainActivity, error, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(response: String) {
                val jsonArray = JSONArray(response)

                (0 until jsonArray.length()).forEach {
                    val jsonElement = jsonArray.get(it) as JSONObject
                    val band = Gson().fromJson(jsonElement.toString(), BandModel::class.java)
                    bands.add(band)
                }

                recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = RecyclerViewAdapter(bands)
                recyclerView.adapter = adapter
            }

        })
    }
}

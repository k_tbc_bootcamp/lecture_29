package com.lkakulia.lecture_29

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.lkakulia.lecture_29.databinding.RecyclerviewItemLayoutBinding

class RecyclerViewAdapter(private val bands: MutableList<BandModel>):
        RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RecyclerviewItemLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.recyclerview_item_layout,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return bands.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(private val binding: RecyclerviewItemLayoutBinding):
            RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.band = bands[adapterPosition]
        }
    }
}
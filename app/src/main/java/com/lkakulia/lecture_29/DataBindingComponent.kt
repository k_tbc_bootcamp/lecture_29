package com.lkakulia.lecture_29

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recyclerview_item_layout.view.*

object DataBindingComponent {
    @JvmStatic
    @BindingAdapter("setResource")
    fun setImage(imageView: ImageView, imgUrl: String) {
        Glide.with(imageView)
            .load(imgUrl)
            .placeholder(R.mipmap.ic_launcher)
            .into(imageView.bandImageView)

    }
}
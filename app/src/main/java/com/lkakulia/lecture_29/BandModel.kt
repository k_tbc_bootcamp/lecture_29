package com.lkakulia.lecture_29

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class BandModel{
    var name = ""
    @SerializedName("img_url")
    var imgUrl = ""
}
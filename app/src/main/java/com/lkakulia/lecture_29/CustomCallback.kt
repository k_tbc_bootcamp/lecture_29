package com.lkakulia.lecture_29

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}
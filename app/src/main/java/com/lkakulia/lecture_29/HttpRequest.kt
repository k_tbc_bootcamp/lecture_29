package com.lkakulia.lecture_29

import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object HttpRequest {

    const val MAIN_INFO = "5ec3ab0f300000850039c29e"

    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_204_NO_CONTENT = 204
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500


    var retrofit = Retrofit.Builder()
        .baseUrl("http://www.mocky.io/v2/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    var service = retrofit.create(
        ApiService::class.java)

    fun getRequest(path: String, callback: CustomCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback: CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            val statusCode = response.code()

            if (statusCode == HTTP_200_OK || statusCode == HTTP_201_CREATED) {
                callback.onResponse(response.body().toString())
            }
            else if (statusCode == HTTP_204_NO_CONTENT) {
                callback.onFailure("No content found")
            }
            else {
                try {
                    val errorJson = JSONObject(response.errorBody()!!.string())
                    var errorString: String? = null

                    if (errorJson.has("error")) {
                        errorString = errorJson.getString("error")
                    }

                    if (statusCode == HTTP_400_BAD_REQUEST || statusCode == HTTP_401_UNAUTHORIZED ||
                        statusCode == HTTP_404_NOT_FOUND
                    ) {
                        callback.onFailure(errorString)
                    }
                    else if (statusCode == HTTP_500_INTERNAL_SERVER_ERROR) {
                        callback.onFailure(errorString)
                    }
                }
                catch (e: JSONException) {
                    callback.onFailure(e.message.toString())
                }
            }
        }

    }

    interface ApiService {
        @GET("{path}")
        fun getRequest(@Path("path") path: String): Call<String>
    }
}